#!/usr/bin/env python3
from zeroconf import Zeroconf, ServiceBrowser
import requests
from pprint import pprint
import argparse
import json
import readline
import sys


def kelvin_to_elgato(elgato=None, kelvin=None):
    slope = -0.04902439024390244
    offset = 486.1707317073171
    if elgato:
        return int((elgato - offset) / slope)
    elif kelvin:
        return int(kelvin * slope + offset)
    else:
        return None


class CommandCompleter(object):
    def __init__(self, commands):
        self.commands = sorted(commands)

    def complete(self, text, state):
        response = None
        if state == 0:
            if text:
                self.matches = [c for c in self.commands if c and c.startswith(text)]
            else:
                self.matches = self.commands[:]
        try:
            response = self.matches[state]
        except IndexError:
            response = None
        return response

class ElgatoListener(object):
    def __init__(self):
      self.lights = {}

    def remove_service(self, zeroconf, type, name):
        try:
            self.lights.pop(name)
        except:
            pass

    def add_service(self, zeroconf, type, name):
        info = zeroconf.get_service_info(type, name)
        #print(f"Service {name} added: {info}")
        self.lights[name] = Light(info)

    def update_service(self, zeroconf, type, name):
        info = zeroconf.get_service_info(type, name)
        #print(f"Service {name} added: {info}")
        self.lights[name] = Light(info)


class Light(object):
    def __init__(self, serviceinfo):
        #pprint(dir(serviceinfo))
        self.serviceinfo = serviceinfo
        self.server = serviceinfo.server
        self.port = serviceinfo.port
        self.light_url = f'http://{self.server}:{self.port}/elgato/lights'
        self.settings_url = f'http://{self.server}:{self.port}/elgato/lights/settings'
        self.info()
        #print(f'On: {self.on}\nTemp: {self.temperature}\nBrightness: {self.brightness}\n')
        
    def info(self):
        r = requests.get(self.light_url).json()
        self.on = r['lights'][0]['on']
        self.brightness = r['lights'][0]['brightness']
        self.temperature = r['lights'][0]['temperature']

    def update(self):
        data = {
          "numberOfLights": 1,
          "lights": [
            {
              "on": self.on,
              "brightness": self.brightness,
              "temperature": self.temperature
            }
          ]
        }
        r = requests.put(self.light_url, data=json.dumps(data))
        self.info()

    def turn_on(self):
        self.info()
        self.on = 1
        self.update()

    def turn_off(self):
        self.info()
        self.on = 0
        self.update()

    def set_brightness(self, brightness):
        if not 0 <= brightness <= 100:
            sys.stderr.write(f'Error, brightness must be in the range 0-100\n')
            return
        self.info()
        self.brightness = brightness
        self.update()

    def set_temperature(self, temperature):
        if not 143 <= temperature <= 344:
            sys.stderr.write(f'Error, temperature must be in the range 143-344\n')
            return
        self.info()
        self.temperature = temperature
        self.update()

    def set_kelvin(self, kelvin):
        if not 2900 <= kelvin <= 7000:
            sys.stderr.write(f'Error, kelvin must be in the range 2900-7000\n')
            return
        self.info()
        self.temperature = kelvin_to_elgato(kelvin=kelvin)
        self.update()

    def print_info(self):
        print(f'Name: {self.serviceinfo.properties[b"md"].decode("utf-8")}\nState: {"on" if self.on else "off"}\nBrightness: {self.brightness}\nTemperature: {self.temperature} ({kelvin_to_elgato(elgato=self.temperature)}K)\n\n')

def process_commands(lights):
    parser = argparse.ArgumentParser(prog='Elgato', description='Fobar')
    subparsers = parser.add_subparsers()
    on_parser = subparsers.add_parser('on', aliases=['1'])
    on_parser.set_defaults(command='on')

    off_parser = subparsers.add_parser('off', aliases=['0'])
    off_parser.set_defaults(command='off')

    print_parser = subparsers.add_parser('print', aliases=['p'])
    print_parser.set_defaults(command='print')

    brightness_parser = subparsers.add_parser('brightness', aliases=['b', 'bright'])
    brightness_parser.set_defaults(command='brightness')
    brightness_parser.add_argument('brightness', type=int)

    temperature_parser = subparsers.add_parser('temperature', aliases=['t', 'temp'])
    temperature_parser.set_defaults(command='temperature')
    temperature_parser.add_argument('temperature', type=int)

    kelvin_parser = subparsers.add_parser('kelvin', aliases=['k', 'kel'])
    kelvin_parser.set_defaults(command='kelvin')
    kelvin_parser.add_argument('kelvin', type=int)

    quit_parser = subparsers.add_parser('quit')
    quit_parser.set_defaults(command='quit')
    quit = False
    while not quit:
        try:
            cmd_str = input('$ ')
        except EOFError:
            quit = True
            continue
        try:
            args = parser.parse_args(cmd_str.split())
        except:
            print('Error')
            continue
        if not hasattr(args, 'command'):
            continue
        if args.command == 'on':
            for light in lights:
                lights[light].turn_on()
        if args.command == 'off':
            for light in lights:
                lights[light].turn_off()
        if args.command == 'brightness':
            for light in lights:
                lights[light].set_brightness(brightness=args.brightness)
        if args.command == 'temperature':
            for light in lights:
                lights[light].set_temperature(temperature=args.temperature)
        if args.command == 'kelvin':
            for light in lights:
                lights[light].set_kelvin(kelvin=args.kelvin)
        if args.command == 'print':
            for light in lights:
                lights[light].print_info()
        if args.command == 'quit':
            quit = True
    sys.stdout.write('\n')

def main():
    commands = [
        'on ',
        'off ',
        'quit ',
        'brightness ',
        'temperature ',
        'kelvin ',
        'print '
    ]
    readline.parse_and_bind('tab: complete')
    readline.set_completer(CommandCompleter(commands).complete)
    zeroconf = Zeroconf()
    listener = ElgatoListener()
    browser = ServiceBrowser(zeroconf, "_elg._tcp.local.", listener)
    try:
        process_commands(listener.lights)
    finally:
        zeroconf.close()

if __name__ == "__main__":
    main()
